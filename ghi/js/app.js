function createCard(name, description, pictureUrl, startDate, endDate, location) {
  return (`
    <div class="card shadow p-3 mb-5 bg-body rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer text-center">
      <small class="text-muted">"${startDate} - ${endDate}"</small>
      </div>
    </div>
  `);
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      alert("Bad response")
    // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        if (detailResponse.ok) {

          const details = await detailResponse.json();
          console.log(details.conference.name)
          const title = details.conference.name;
          const location = details.conference.location.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts).toLocaleDateString();
          const endDate = new Date(details.conference.ends).toLocaleDateString();
          const html = createCard(title, description, pictureUrl, startDate, endDate, location);


          const columns = document.querySelectorAll('.col')
          console.log(columns)
          columns[data.conferences.indexOf(conference) % 3].innerHTML += html

        }
      }
    }
  } catch (e) {
    alert("An error has occured")
  // Figure out what to do if an error is raised
  }

});


