import {NavLink} from "react-router-dom";

function Nav() {
    return (
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">Conference GO!</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
            <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li class="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/locations/new">New location</NavLink>
            </li>
            <li class="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/conferences/new">New Conference</NavLink>
            </li>
            <li class="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/presentations/new">New Presentation</NavLink>
            </li>
            <li>
            <NavLink className="nav-link" aria-current="page" to="/attendees/new">Attend Conference</NavLink>
            </li>
            <li>
            <NavLink className="nav-link" aria-current="page" to="/attendees">Attendees</NavLink>
            </li>
          </ul>
          <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search conferences" aria-label="Search"/>
            <button class="btn btn-outline-success me-2" type="submit">Search</button>
          </form>
        </div>
      </div>
    </nav>

    );


}

  export default Nav;
